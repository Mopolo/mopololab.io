# My portfolio

This website is built with [Jigsaw](https://jigsaw.tighten.co/) and hosted on this repo.

You can visit it at <https://mopolo.fr/>

## Local setup

```shell
# Clone the repo
$ git clone git@gitlab.com:Mopolo/mopolo.gitlab.io.git

# Go inside the new directory
$ cd portfolio

# Install dependencies
$ composer install
$ yarn

# During dev
$ yarn run watch

# Make a build
$ yarn run production
```
