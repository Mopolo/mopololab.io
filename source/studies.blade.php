@extends('_layouts.master')

@section('title', 'Studies')

@section('body')
    <div class="main">
        <h2 class="section-title">Studies</h2>

        <p>
            I studied computer science for 5 years, 4 in France and the last one
            in Montréal, Canada.
        </p>

        <div class="studies-container">
            <dl class="dl-horizontal">
                <dt>
                    @include('partials.dates', ['from' => '2013', 'to' => '2014'])
                </dt>
                <dd>Supinfo Montréal</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>@include('partials.dates', ['from' => '2012', 'to' => '2013'])</dt>
                <dd>Supinfo Paris</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>@include('partials.dates', ['from' => '2011', 'to' => '2012'])</dt>
                <dd>Licence in IT in Amiens</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>@include('partials.dates', ['from' => '2009', 'to' => '2011'])</dt>
                <dd>DUT in IT in Amiens</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>2009</dt>
                <dd>Baccalauréat Scientifique</dd>
            </dl>
        </div>
    </div>
@endsection
