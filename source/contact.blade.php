@extends('_layouts.master')

@section('title', 'Contact')

@section('body')
    <div class="main">
        <h2 class="section-title">Contact</h2>

        <p>
            You can contact me via email at
            <a href="mailto:hello@mopolo.net">hello@mopolo.net</a>
        </p>

        <p>
            My Linkedin profile is
            <a href="https://www.linkedin.com/in/nathan-boiron-79785a60/">here</a>
        </p>

        <p>
            I'm also on Twitter:
            <a href="https://twitter.com/Lopom">@Lopom</a>
        </p>
    </div>
@endsection
