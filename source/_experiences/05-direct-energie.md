---
id: direct-energie
title: "Direct Energie"
link: "https://total.direct-energie.com/"
start: "2016-10-01"
end: "now"
location: Paris
---

Direct Energie is a french electric utility company which produces and distributes
gas and electricity.
