---
id: ratp
title: "RATP"
link: "https://www.ratp.fr/"
start: "2011-03-01"
end: "2011-06-30"
location: Paris
---

The Régie Autonome des Transports Parisiens (RATP) is the public transport operator
of Paris and it's surroudings.
