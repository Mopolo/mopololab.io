---
id: globalis
title: "Globalis"
link: "https://www.globalis-ms.com/"
start: "2015-12-01"
end: "2016-06-30"
location: Paris
---

Globalis is an IT service company based in Paris. I was on mission at [L'Obs][obs], a french
online and paper magazine.

[obs]: https://tempsreel.nouvelobs.com/
