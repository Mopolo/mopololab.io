---
id: morningcroissant
title: "MorningCroissant"
link: "https://www.morningcroissant.fr/"
start: "2013-07-01"
end: "2013-09-30"
location: Paris
---

MorningCroissant is a house rental platform for your or other people's places.
