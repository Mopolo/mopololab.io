---
experience: morningcroissant
title:  "MorningCroissant"
link: "https://www.morningcroissant.fr/"
images:
  - /assets/images/projects/mc-1.jpg
  - /assets/images/projects/mc-2.jpg
  - /assets/images/projects/mc-3.jpg
  - /assets/images/projects/mc-4.jpg
images_layout: site
techs:
  - PHP
  - Zend 1
---

I did a six months internship in this company in Paris.

I've done many things such as:

 - A drag&amp;drop uploader for photos
 - HTML integration of psd models
 - SEO (sitemaps and cities description extracted from Wikipedia via crawling)
 - Improvements on the internal search engine
 - A/B testing solution
 - Visual calendar to choose availabilities
