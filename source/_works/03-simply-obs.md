---
experience: osedea
title:  "Simply Obstetrics"
link: "https://www.simplyobstetrics.com/"
images:
  - /assets/images/projects/simply-obs-1.jpg
  - /assets/images/projects/simply-obs-2.jpg
  - /assets/images/projects/simply-obs-3.jpg
images_layout: mobile
techs:
  - AngularJS
  - Ionic
---

This is the first project I worked on during my internship at [Osedea](https://osedea.com).

It's an app for doctors made using [Ionic](https://ionicframework.com/). All the content is static.
