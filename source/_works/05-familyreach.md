---
experience: osedea
title:  "FamilyReach Give"
link: "https://familyreach.org/"
images:
  - /assets/images/projects/fr-1.jpg
  - /assets/images/projects/fr-2.jpg
  - /assets/images/projects/fr-3.jpg
  - /assets/images/projects/fr-4.jpg
  - /assets/images/projects/fr-5.jpg
images_layout: mobile
techs:
  - PHP
  - Laravel 4
  - AngularJS
  - Bootstrap
  - Ionic
  - Beanstalk
---

FamilyReach Give is a crowdfunding app to help families who have a child with cancer.

#### The backend

- Built with Laravel 4 and AngularJS
- The association people can manage families, payments and parameters
- Data is exposed via a REST APi

#### The app

- Built with Ionic
- Listing of families
- Payments in-app using Stripe
