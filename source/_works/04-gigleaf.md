---
experience: osedea
title:  "Gigleaf"
link: "https://gigleaf.me/"
images:
  - /assets/images/projects/gigleaf-1.jpg
  - /assets/images/projects/gigleaf-2.jpg
  - /assets/images/projects/gigleaf-3.jpg
images_layout: site
techs:
  - PHP
  - Laravel 4 & 5
  - AngularJS
  - Bootstrap
  - Beanstalk
---

Gigleaf is a project built at Osedea. This is a website on which Creative Directors
can view and hire Freelancers.

 - First project made with Laravel
 - Single Page Application
 - Queued jobs using Beanstalk
 - Task scheduling using Laravel scheduler
 - Emailing using Mandrill API
