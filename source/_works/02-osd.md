---
experience: osedea
title:  "Osd - a CLI tool"
techs:
  - Node.js
  - Cordova
  - Git
  - Android
  - iOs
---

Osd is a tool for the terminal that I started building and kept working on for the
entire time I was in the company.

It was first built as a single bash script but was quickly rebuilt using Node.js

It allowed to automate some of the company's processes:

- SSH aliases management
- Projects version management
 + creation of Git tags
 + update of composer.json and package.json files
- Cordova projects compilation to iOs or Android
- Time management
- Project skeleton generation
- Code snippets generation
