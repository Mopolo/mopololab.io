---
experience: osedea
anchor: "morcare"
title:  "Morcare"
link: "https://morcare.ca/"
images:
  - /assets/images/projects/morcare-1.jpg
  - /assets/images/projects/morcare-2.jpg
images_layout: site
techs:
  - PHP
  - Zend 2
  - AngularJS
  - Bootstrap
---

Morcare is an international version of WeSpeakStudent.
