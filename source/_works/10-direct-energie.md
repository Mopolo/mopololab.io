---
experience: direct-energie
link: "https://total.direct-energie.com/"
techs:
  - PHP
  - TYPO3
  - Laravel
  - Doctrine
  - PHPStan
  - PHPUnit
---

I worked on both the website and on the API:

#### The Website

- Configuration (pages, options, etc.)
- Plugins for custom content
 + Subscription form with complex steps
 + Consumption simulator
 + Client login flow
 + Admin login flow
- Backend modules
 + Logging tool for WebServices and errors
 + Internal tools (e.g. monitoring)

#### Custom PHP Micro framework

This micro framework was developed to access the company's data the same
way in the website and the API.

- Composer for dependencies
- Standalone Doctrine setup for several databases
- Custom library to call SOAP and HTTP WebServices
- Enum system to handle business "magic numbers"
- Documentation in markdown
- Unit and feature tests using PHPUnit
- Code analysis with PHPStan

#### API

- Built using Laravel
- Uses the custom micro framework
