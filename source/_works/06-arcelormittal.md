---
experience: osedea
title:  "Arcelormittal"
techs:
  - PHP
  - Zend 2
  - AngularJS
  - Bootstrap
  - Git
---

We built a tool to help with vehicle usage planing in a mine in Canada.

The assigning of vehicles was based on a lot of parameters and was done semi-manually
using Excel.

The tool consists of a web app built with Zend 2 and AngularJS. It was able to assign
vehicles automatically based on different criteria.
