---
experience: globalis
anchor: "nouvelobs"
title:  "Nouvel Obs"
link: "https://tempsreel.nouvelobs.com/"
images_layout: site
techs:
  - PHP
  - SVN
  - Git
  - Bootstrap
  - Docker
---

The Nouvel Obs is a french paper and online magazine.

I worked both on the frontend and the backend:

- Home made PHP framework
- Started and helped the transition from SVN to Git
- Setup of an internal Gitlab
- Creation of a backoffice to manage zones (parts of pages) to help 
redactors organise layouts with a visual UI
- Worked on the complete refactor of the Article page
- Discovery of Docker as a dev environment
