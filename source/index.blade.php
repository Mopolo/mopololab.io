@extends('_layouts.master')

@section('title', 'Home')

@section('body')
    <div class="main">
        <h2 class="section-title visible-block-desk">Welcome</h2>

        <div>
            <div class="left">

                <pre class="normal about-tour" aria-hidden="true">
      .
     .|.
     |||
     |||
     |||
     |||
     j_I
    .)_(.
    |===|
    /___\
   //___\\
  /=======\
 / .-"""-. \
|__|     |__|
        </pre>

            </div>

            <div class="about-top">
                <p>
                    My name is Nathan Boiron and I'm <span id="age">27</span>.
                </p>

                <p>
                    I was born in Bordeaux (France) and I studied computer science to make websites and stuff!
                </p>
            </div>
        </div>
    </div>
@endsection
