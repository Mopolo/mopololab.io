---
layout: project
title:  "Craftworld"
link: "https://craftworld.fr/"
categories: project
images:
  - /assets/images/projects/cw-1.jpg
  - /assets/images/projects/cw-2.jpg
  - /assets/images/projects/cw-3.jpg
  - /assets/images/projects/cw-4.jpg
  - /assets/images/projects/cw-5.jpg
  - /assets/images/projects/cw-6.jpg
images_layout: site
techs:
  - PHP
  - Java
  - Debian
  - Git
---

CraftWorld was a french Minecraft server created in 2011 and closed in 2014.

This project was both a hobby and a way to practice and experiment technologies. The game and the website were hosted on two different machines.

I developed an API and tools to create interactions between the website and the game.

Players could administrate some aspects of the game on the website such as their ingame money and their rank.

I also built a full control panel for the staff team which allowed them to administrate the server without being logged in the game.
They could see the console log, execute ingame commands, kick/ban players, install new features, start/stop the server, etc.

With this I've learned many things such as:

 - how to create a development platform using [Github](https://github.com/)
 - install, configure and maintain a debian server
 - project management with [Trello](https://trello.com/)
