(function() {

    function calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);

        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    var ageElement = document.getElementById('age');

    if (ageElement) {
        var age = calculateAge(new Date(1990, 7, 6, 0, 0, 0, 1));

        document.getElementById('age').innerHTML = age + '';
    }

    function duckface(face) {
        document.getElementById('duckface').innerHTML = face;
    }

    document.getElementById('duck').addEventListener('click', function() {
        setTimeout(function() { duckface('>'); }, 200);
        setTimeout(function() { duckface('-'); }, 500);
        setTimeout(function() { duckface('>'); }, 800);
        setTimeout(function() { duckface('-'); }, 1100);
    });

})();
