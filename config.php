<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use TightenCo\Jigsaw\PageVariable;

return [

    'baseUrl' => '',
    'production' => false,

    'collections' => [
        'experiences' => [],
        'works' => [],
        'projects' => [],
    ],

    'menu' => function (PageVariable $page, string $section) {
        if ((empty($section) && empty($page->getPath()))
            || (str_contains($page->getPath(), $section) && !empty($section))
        ) {
            return 'active';
        }

        return '';
    },

    'getWorks' => function (PageVariable $page, Collection $works, $experience) {
        return $works
            ->filter(function ($work) use ($experience) {
                return $work->experience === $experience;
            })
            ->reverse();
    },

    'period' => function(PageVariable $page, $start, $end) {
        if (empty($start) || empty($end)) {
            return '';
        }

        $start = Carbon::parse($start)->format('F Y');

        $end = $end === 'now'
            ? 'Now'
            : Carbon::parse($end)->format('F Y');

        return "$start &mdash; $end";
    },

];
